import { Component } from '@angular/core';
import {AppSettings} from "../../app.settings";

@Component({
  selector: 'app-tb-login-layout',
  template: `    
    <div class="row form-container" >
      <app-tb-login-slider></app-tb-login-slider>
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [],
  encapsulation:AppSettings.ENCAPSULATE
})
export class TbLoginLayoutComponent {}
