import {Component,OnInit} from '@angular/core';
import {TbCommonService} from '../../tb-service/tb-common.service';
import {AppSettings} from "../../app.settings";


@Component({
  selector: 'app-tb-home-layout',
  template: `
    <app-tb-header></app-tb-header>
    <div id="page">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [],
  providers: [TbCommonService],
  encapsulation:AppSettings.ENCAPSULATE
})

export class TbHomeLayoutComponent implements OnInit {



  constructor( private userService: TbCommonService) {

  }

  ngOnInit() {

  }

  ngAfterViewInit() {

  }




}
