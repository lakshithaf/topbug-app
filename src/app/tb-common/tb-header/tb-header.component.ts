import {Component, OnInit, TemplateRef} from '@angular/core';
import {Router} from '@angular/router';
import {AppSettings} from '../../app.settings';
import {TbHeaderService} from './tb-header.service';
import {TbCommonService} from '../../tb-service/tb-common.service';
import {TbLoaderService} from '../tb-loader/tb-loader.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-tb-header',
  templateUrl: './tb-header.component.html',
  styles: [],
  providers: [TbHeaderService],
  encapsulation: AppSettings.ENCAPSULATE
})
export class TbHeaderComponent implements OnInit {
  public data;
  public st_data;
  public st_name: string;
  public st_image: string;
  public m_navbar: string = "deactive";
  public modalRef: BsModalRef;
  public config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'logout_confirm_box'
  };

  constructor(private route: Router,
              private commonService: TbCommonService,
              private _service: TbHeaderService,
              private tbLoaderService: TbLoaderService,
              private _modalService: BsModalService) {
    this.st_name = 'Loading...';
    this.st_image = 'assets/images/noavatar.png';
  }

  ngOnInit() {
    let token = localStorage.getItem('token');
    this._service.getProfileData(token).subscribe(data => {
      this.st_data = data;
      this.st_name = this.st_data.data[0].firstName;
      this.st_image = AppSettings.FILE_MANAGER_API_ENDPOINT + "/profile_image/tumbline/" + token;
    })

  }

  loadDashboard() {
    if (this.m_navbar == "active") {
      this.m_navbar = "deactive";
      this.tbLoaderService.displaySideNavLoader(false);
    }
    this.route.navigateByUrl('portal/dashboard');
  }

  loadNavLinks(path) {

    if (this.m_navbar == "active") {
      this.m_navbar = "deactive";
      this.tbLoaderService.displaySideNavLoader(false);
    }

    if (path == "msg") {
      this.route.navigateByUrl('portal/dashboard');
    }
    if (path = "notify") {
      this.route.navigateByUrl('portal/dashboard');
    }
    if (path = "profile") {
      this.route.navigateByUrl('portal/dashboard');
    }
  }

  showNotifications() {

  }

  onLogout() {

    this.modalRef.hide();
    this.tbLoaderService.displayMainLoader(true);
    this.commonService.endSession().subscribe(data => {
      this.data = data;
      if (this.data.status == 200 && this.data.message == "Success") {
        localStorage.removeItem("token");
        this.tbLoaderService.displayMainLoader(false);
        this.route.navigateByUrl('/');
        this.tbLoaderService.displaySideNavLoader(false);
      }

    })

  }

  showSideBar() {
    if (this.m_navbar == "deactive") {
      this.tbLoaderService.displaySideNavLoader(true);
      this.m_navbar = "active";
    } else {

      this.m_navbar = "deactive";
      this.tbLoaderService.displaySideNavLoader(false);
    }

  }

  public openModal(template: TemplateRef<any>) {

    this.modalRef = this._modalService.show(template, Object.assign({}, this.config));}

}
