import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {AppSettings} from '../../app.settings';
import {Router, ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

@Injectable()
export class TbHeaderService {
  public json;

  constructor(private _http: HttpClient) {

  }

  getProfileData(token) {

    return this._http.get(AppSettings.ST_PORTAL_API_ENDPOINT + '/student/info/'+token)

  }


}
