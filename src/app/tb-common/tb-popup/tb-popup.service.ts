import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class TbPopupService {
  constructor(private route: Router) {

  }
  loadDashboard(){
    this.route.navigateByUrl('portal/dashboard');
  }
}
