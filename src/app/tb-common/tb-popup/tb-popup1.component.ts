import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TbPopupService} from './tb-popup.service';
import {AppSettings} from "../../app.settings";
import {BsModalRef} from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'popup-dashboard',
  templateUrl: './tb-popup1.component.html',
  styles: [],
  encapsulation:AppSettings.ENCAPSULATE

})
export class TbPopup1Component implements OnInit {
  public onClose: Subject<boolean>;

  constructor(private _bsModalRef: BsModalRef,private tbPopupService: TbPopupService) {

  }

  public ngOnInit(): void {
    this.onClose = new Subject();
  }

  public onConfirm(): void {
    this.onClose.next(true);
    this._bsModalRef.hide();
  }

  public onCancel(): void {
    this.onClose.next(false);
    this._bsModalRef.hide();
  }


}
