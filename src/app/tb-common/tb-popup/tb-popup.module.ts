import {NgModule} from '@angular/core';
import {TbPopup1Component} from "./tb-popup1.component";

import {TbPopupService} from "./tb-popup.service";
import {BsModalRef} from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [
    TbPopup1Component

  ],
  imports: [

  ],
  exports: [
    TbPopup1Component
  ],
  providers: [TbPopupService,BsModalRef],
  bootstrap: []
})

export class TbPopupModule {
}
