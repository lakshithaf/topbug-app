import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TbLoaderService} from './tb-loader.service';
import {AppSettings} from "../../app.settings";


@Component({
  selector: 'app-tb-loader',
  templateUrl: './tb-loader.component.html',
  styles: [],
  encapsulation:AppSettings.ENCAPSULATE

})
export class TbLoaderComponent implements OnInit {
  public load: boolean = false;
  public sideNavLoad: boolean = false;

  constructor(private tbLoaderService: TbLoaderService) {

  }

  ngOnInit() {
    this.tbLoaderService.status.subscribe((val: boolean) => {
      this.load = val;
    });
    this.tbLoaderService.statusNav.subscribe((val: boolean) => {
      this.sideNavLoad= val;
    });

  }


}
