import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class TbLoaderService {
  public status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public statusNav: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  displayMainLoader(value: boolean) {
    this.status.next(value);
  }
  displaySideNavLoader(value: boolean) {
    this.statusNav.next(value);
  }
}
