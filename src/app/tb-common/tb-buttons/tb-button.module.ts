import {NgModule} from '@angular/core';
import {TbButton1Component} from "./tb-button1.component";

import {TbButtonService} from "./tb-button.service";



@NgModule({
  declarations: [
    TbButton1Component

  ],
  imports: [

  ],
  exports: [
    TbButton1Component
  ],
  providers: [TbButtonService],
  bootstrap: []
})

export class TbButtonModule {
}
