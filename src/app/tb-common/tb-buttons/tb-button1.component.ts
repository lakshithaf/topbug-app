import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TbButtonService} from './tb-button.service';
import {AppSettings} from "../../app.settings";


@Component({
  selector: 'button-dashboard',
  template: `<button class="btn backn"  (click)="backToMain()"><i class="icon-left-arrow"></i></button>`,
  styles: [],
  encapsulation:AppSettings.ENCAPSULATE

})
export class TbButton1Component implements OnInit {
  public load: boolean = false;

  constructor(private tbButtonService: TbButtonService) {

  }

  ngOnInit() {

  }
  backToMain(){

    this.tbButtonService.loadDashboard()

  }


}
