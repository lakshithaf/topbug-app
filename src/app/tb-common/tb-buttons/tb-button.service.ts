import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class TbButtonService {
  constructor(private route: Router) {

  }
  loadDashboard(){
    this.route.navigateByUrl('portal/dashboard');
  }
}
