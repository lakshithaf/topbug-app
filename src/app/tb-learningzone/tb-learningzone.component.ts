import {Component, OnInit} from '@angular/core';
import {AppSettings} from '../app.settings';
import {TbLoaderService} from "../tb-common/tb-loader/tb-loader.service";
import {TbLearningzoneService} from "./tb-learningzone.service";

@Component({
  selector: 'app-tb-learningzone',
  templateUrl: './tb-learningzone.component.html',
  styleUrls: ['./tb-learningzone.component.scss'],
  providers: [TbLearningzoneService],
  encapsulation:AppSettings.ENCAPSULATE
})
export class TbLearningzoneComponent implements OnInit {
  public data;
  public model;
  constructor(private tbLoaderService: TbLoaderService,private _service:TbLearningzoneService) {
  }

  ngOnInit() {
    if (sessionStorage.getItem('sub_comp') == null) {
      let token = localStorage.getItem('token');
      this._service.getSubject(token).subscribe(data => {
        this.data = data;
        this.generateSubjects(this.data);
      })
    }else{
      this.generateSubjects(JSON.parse(sessionStorage.getItem('sub_comp')));
    }
  }

  ngAfterViewInit() {

    setTimeout(() => {
      this.tbLoaderService.displayMainLoader(false);
    }, AppSettings.MAIN_LOADER_TIMEOUT);

  }

  generateSubjects(data) {

    if (data.status == 200 && data.data.length > 0) {
      this.model = data.data;
      for (let i in this.model) {

        if (this.model[i].subject_name == "Maths") {
          this.model[i].bg_color = '#22c8f8';
          this.model[i].img_url = 'assets/images/maths.png';
        }
        if (this.model[i].subject_name == "Biology") {
          this.model[i].bg_color = '#039479';
          this.model[i].img_url = 'assets/images/bio.png';
        }
        if (this.model[i].subject_name == "Chemistry") {
          this.model[i].bg_color = '#e2a21c';
          this.model[i].img_url = 'assets/images/chemistry.png';
        }
        if (this.model[i].subject_name == "Physics") {
          this.model[i].bg_color = '#c13d3c';
          this.model[i].img_url = 'assets/images/physics.png';
        }
        if (this.model[i].subject_avail == 0) {
          this.model[i].onclick = 'disabled_Subject()';
          this.model[i].addClass = 'disabled';
          this.model[i].style = '#ccc';
          this.model[i].stylebg = '#aaa';
          this.model[i].bg_color = '#ccc';
        }
        if (this.model[i].subject_avail != 0) {
          this.model[i].onclick = '';
          this.model[i].addClass = 'hvr-grow materialize';
          this.model[i].style = '';
          this.model[i].stylebg = this.model[i].color;

        }

      }

      setTimeout(() => {
        this.tbLoaderService.displayMainLoader(false);
      }, AppSettings.MAIN_LOADER_TIMEOUT);

    }
    else{

    }
  }

}
