import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbLearningzoneComponent } from './tb-learningzone.component';

describe('TbLearningzoneComponent', () => {
  let component: TbLearningzoneComponent;
  let fixture: ComponentFixture<TbLearningzoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbLearningzoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbLearningzoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
