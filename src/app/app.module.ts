import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SailsModule} from "angular2-sails";
import {TbBoostrapModule} from './tb-common/tb-boostrap/tb-boostrap.module';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';

import {AppRoutingModule} from './app-routing.module';
import {TbHomeLayoutComponent} from './tb-common/tb-layout/tb-layout-home.component';
import {TbLoginLayoutComponent} from './tb-common/tb-layout/tb-layout-login.component';
import {TbLoaderComponent} from './tb-common/tb-loader/tb-loader.component';
import {TbHeaderComponent} from './tb-common/tb-header/tb-header.component';
import {TbLoginComponent} from './tb-users/tb-login/tb-login.component';
import {TbSignupComponent} from './tb-users/tb-signup/tb-signup.component';
import {TbPwchangeComponent} from './tb-users/tb-pwchange/tb-pwchange.component';
import {TbSliderComponent} from './tb-common/tb-slider/tb-slider.component';
import {TbHomeComponent} from './tb-home/tb-home.component';
import {TbDashboardComponent} from './tb-dashboard/tb-dashboard.component';
import {TbLearningzoneComponent} from './tb-learningzone/tb-learningzone.component';
import {TbChallengeComponent} from './tb-challenge/tb-challenge.component';
import {TbAssessmentcenterComponent} from './tb-assessmentcenter/tb-assessmentcenter.component';
import {TbPreformancereviewComponent} from './tb-preformancereview/tb-preformancereview.component';



import {TbLoaderService} from './tb-common/tb-loader/tb-loader.service';
import {AppService} from './app.service';
import {TbCommonService} from './tb-service/tb-common.service';

import {TbGuardsModule} from './tb-guards/tb-guards.module';
import {TbButtonModule} from './tb-common/tb-buttons/tb-button.module';
import {TbPopupModule} from './tb-common/tb-popup/tb-popup.module';


@NgModule({
  declarations: [
    AppComponent,
    TbHomeLayoutComponent,
    TbLoginLayoutComponent,
    TbHeaderComponent,
    TbLoaderComponent,
    TbLoginComponent,
    TbSignupComponent,
    TbPwchangeComponent,
    TbSliderComponent,
    TbHomeComponent,
    TbDashboardComponent,
    TbLearningzoneComponent,
    TbChallengeComponent,
    TbAssessmentcenterComponent,
    TbPreformancereviewComponent,


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    SailsModule.forRoot(),
    TbBoostrapModule,
    FormsModule,
    AppRoutingModule,
    TbGuardsModule,
    TbButtonModule,
    TbPopupModule

  ],
  providers: [AppService,TbLoaderService,HttpClientModule,TbCommonService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
