import {Injectable} from '@angular/core'
import {Observable} from 'rxjs/Observable'
import {Subject} from 'rxjs/Subject'
import {AppSettings} from '../app.settings';

@Injectable()
export class IoService {
  private _ioMessage$:Subject<{}>;
  private apiRoot:string=AppSettings.ST_PORTAL_API_ENDPOINT;


  constructor() {
    this._ioMessage$ = <Subject<{}>>new Subject();
    this.registerSailsListener();
  }

  get ioMessage$() {
    return this._ioMessage$.asObservable();
  }

  registerSailsListener():void {
    self["io"].socket.on('message', (data) => {
      this._ioMessage$.next(data.data);
    });
  }

  subscribeToSails() {
    self["io"].socket.request({
      method: 'get',
      url: AppSettings.ADMIN_PORTAL_API_ENDPOINT + '/socket/createInstance',
      headers: {authorization: AppSettings.AUTH_TOKEN}
    }, function (resData, jwres) {
      if (jwres.error) {
        console.log(jwres.statusCode);
        return;
      }
      console.log(jwres.statusCode);
    });


  }

  sendMessage(submission:{}) {
    self["io"].socket.get('/room/submit', {
      message: submission["message"]
    });
  }

  unsubscribeToSails() {
    self["io"].socket.get('/room/leave');
    this._ioMessage$.next({
      message: "Left Chat Room"
    });
  }
}
