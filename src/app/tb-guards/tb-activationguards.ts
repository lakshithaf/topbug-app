import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {TbGuardsService} from './tb-guards.service';
import {TbLoaderService} from "../tb-common/tb-loader/tb-loader.service";
import {Router, ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/map';


@Injectable()
export class OnlyLoggedInUsersGuard implements CanActivate {

  constructor(private userService: TbGuardsService,
              private route: Router, router: ActivatedRoute,
              private tbLoaderService: TbLoaderService) {
  }

  public canActivate() {

    this.tbLoaderService.displayMainLoader(true);
    let token = localStorage.getItem('token');
    if (token) {
      return this.userService.isLogged(token)
    }
    else {
      this.route.navigate(["/"]);
      return false;

    }

  }
}
