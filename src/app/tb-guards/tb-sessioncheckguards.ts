import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {TbGuardsService}   from './tb-guards.service';
import {Router, ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/map';


@Injectable()
export class SessionCheckGuard implements CanActivate {

  constructor(private userService:TbGuardsService, private route:Router, router:ActivatedRoute) {
  }

  public canActivate() {

    let token = localStorage.getItem('token');
    if (token) {
      return this.userService.sessionCheck(token)
    }
    else {
      return true;

    }

  }
}
