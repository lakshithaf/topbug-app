import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {AppSettings} from '../app.settings';
import {Router, ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {TbLoaderService} from "../tb-common/tb-loader/tb-loader.service";

const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

@Injectable()
export class TbGuardsService {

  private json;

  constructor(private _http: HttpClient,
              private route: Router,
              router: ActivatedRoute,
              private tbLoaderService: TbLoaderService) {

  }

  isLogged(token): Observable<boolean> {
    return this.checkToken(token).map(data => {
      this.json = data;
      if (this.json.data == "valid") {
        return true;
      }
      else {
        setTimeout(() => {
          this.tbLoaderService.displayMainLoader(false);
        }, AppSettings.MAIN_LOADER_TIMEOUT);
        this.route.navigate(["/"]);
        return false;
      }
    })
  }

  sessionCheck(token): Observable<boolean> {
    return this.checkToken(token).map(data => {
      this.json = data;
      if (this.json.data == "valid") {
        this.route.navigate(["portal/dashboard"]);
        return false;
      } else {
        return true;
      }
    })
  }

  public checkToken(token) {

    return this._http.get(AppSettings.ST_PORTAL_API_ENDPOINT + '/checktoken/' + token, httpOptions)
  }

  public endSession() {

    let token = localStorage.getItem('token');
    return this._http.get(AppSettings.ADMIN_PORTAL_API_ENDPOINT + '/api/endSession/' + token, httpOptions)

  }


}
