import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {OnlyLoggedInUsersGuard}   from './tb-activationguards';
import {SessionCheckGuard}   from './tb-sessioncheckguards';
import {TbGuardsService}   from './tb-guards.service';

@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [OnlyLoggedInUsersGuard,SessionCheckGuard,HttpClientModule,TbGuardsService],
  bootstrap: []
})
export class TbGuardsModule {
}
