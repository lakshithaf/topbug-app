import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbPreformancereviewComponent } from './tb-preformancereview.component';

describe('TbPreformancereviewComponent', () => {
  let component: TbPreformancereviewComponent;
  let fixture: ComponentFixture<TbPreformancereviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbPreformancereviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbPreformancereviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
