import {Component, OnInit} from '@angular/core';
import * as c3 from 'c3';
import {AppSettings} from '../app.settings';
import {TbLoaderService} from "../tb-common/tb-loader/tb-loader.service";

@Component({
  selector: 'app-tb-preformancereview',
  templateUrl: './tb-preformancereview.component.html',
  styleUrls: ['./tb-preformancereview.component.scss'],
  encapsulation: AppSettings.ENCAPSULATE
})
export class TbPreformancereviewComponent implements OnInit {

  constructor(private tbLoaderService: TbLoaderService) {
  }

  ngOnInit() {

  }

  ngAfterViewInit() {

    setTimeout(() => {
      this.tbLoaderService.displayMainLoader(false);
    }, AppSettings.MAIN_LOADER_TIMEOUT);

    let chart = c3.generate({
      bindto: '#time_on_portal',
      data: {
        columns: [
          ['data1', 30, 200, 100, 400, 150, 250],
          ['data2', 50, 20, 10, 40, 15, 25]
        ]
      }
    });
  }


}
