import {NgModule, ModuleWithProviders} from '@angular/core';
import {RouterModule, Route, Routes} from '@angular/router';

import {TbHomeLayoutComponent} from './tb-common/tb-layout/tb-layout-home.component';
import {TbLoginLayoutComponent} from './tb-common/tb-layout/tb-layout-login.component';
import {TbLoginComponent} from './tb-users/tb-login/tb-login.component';
import {TbSignupComponent} from './tb-users/tb-signup/tb-signup.component';
import {TbPwchangeComponent} from './tb-users/tb-pwchange/tb-pwchange.component';
import {TbHomeComponent} from './tb-home/tb-home.component';
import {TbDashboardComponent} from './tb-dashboard/tb-dashboard.component';
import {TbLearningzoneComponent} from './tb-learningzone/tb-learningzone.component';
import {TbChallengeComponent} from './tb-challenge/tb-challenge.component';
import {TbAssessmentcenterComponent} from './tb-assessmentcenter/tb-assessmentcenter.component';
import {TbPreformancereviewComponent} from './tb-preformancereview/tb-preformancereview.component';

import {OnlyLoggedInUsersGuard} from './tb-guards/tb-activationguards';
import {SessionCheckGuard} from './tb-guards/tb-sessioncheckguards';


const appRoutes: Routes = [
  {
    path: 'portal',
    component: TbHomeLayoutComponent,
    canActivate: [OnlyLoggedInUsersGuard],
    children: [
      {
        path: '',
        component: TbDashboardComponent,
        canActivate: [OnlyLoggedInUsersGuard],
      },
      {
        path: 'dashboard',
        component: TbDashboardComponent,
        canActivate: [OnlyLoggedInUsersGuard],
      },
      {
        path: 'learning-zone',
        component: TbLearningzoneComponent,
        canActivate: [OnlyLoggedInUsersGuard],
      },
      {
        path: 'challenge',
        component: TbChallengeComponent,
        canActivate: [OnlyLoggedInUsersGuard],
      },
      {
        path: 'assessment-center',
        component: TbAssessmentcenterComponent,
        canActivate: [OnlyLoggedInUsersGuard],

      },
      {
        path: 'assessment-center/:subId',
        component: TbAssessmentcenterComponent,


      },
      {
        path: 'assessment-center/:subId/:testType',
        component: TbAssessmentcenterComponent,


      },
      {
        path: 'performance-review',
        component: TbPreformancereviewComponent,
        canActivate: [OnlyLoggedInUsersGuard],
      }
    ]
  },
  {
    path: '',
    component: TbLoginLayoutComponent,
    canActivate: [SessionCheckGuard],
    children: [
      {
        path: '',
        component: TbLoginComponent
      },
      {
        path: 'login',
        component: TbLoginComponent
      },
      {
        path: 'sign-up',
        component: TbSignupComponent
      },
      {
        path: 'pw-change',
        component: TbPwchangeComponent
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}

]

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
