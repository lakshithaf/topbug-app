import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppSettings} from '../app.settings';
import {IoService} from "../tb-service/tb-socket.io.service";
import {TbLoaderService} from "../tb-common/tb-loader/tb-loader.service";
import {TbDashboardService} from './tb-dashboard.service';


@Component({
  selector: 'app-tb-dashboard',
  templateUrl: './tb-dashboard.component.html',
  styleUrls: ['./tb-dashboard.component.scss'],
  providers: [TbDashboardService],
  encapsulation:AppSettings.ENCAPSULATE
})
export class TbDashboardComponent implements OnInit {
  public data;
  public model;
  public year;

  constructor(private route: Router,
              private _ioService: IoService,
              private tbLoaderService: TbLoaderService,
              private _service: TbDashboardService) {

    this.year=new Date().getFullYear();
  }

  ngOnInit() {
    let token = localStorage.getItem('token');
    this._ioService.subscribeToSails();
    this._service.getDashboardComponents(token).subscribe(data => {
      this.data = data;
      this.generateDashboard(this.data)
    });

  }


  loadComponent(componant) {

    if (componant == "lnzn") {

      this.route.navigateByUrl('portal/learning-zone');

    }
    if (componant == "aszn") {

      this.route.navigateByUrl('portal/assessment-center');

    }
    if (componant == "qzch") {
      this.route.navigateByUrl('portal/challenge');

    }
    if (componant == "pmrw") {

      this.route.navigateByUrl('portal/performance-review');
    }

  }

  ngAfterViewInit() {

  }

  generateDashboard(data) {

    if (data.status == 200 && data.data.length > 0) {
      this.model = data.data;
      for (let i in this.model) {

        if (this.model[i].componant != "mesg" && this.model[i].componant != "ad") {

          if (this.model[i].componant == "lnzn") {
            this.model[i].color = '#22c8f8';
            this.model[i].image = 'assets/images/dashboard-images/learning-zone.png';
            this.model[i].clickfn = "loadLearningZone()";
            this.model[i].block = (parseInt(i) + 1);
          }
          if (this.model[i].componant == "aszn") {
            this.model[i].color = '#c05a67';
            this.model[i].image = 'assets/images/dashboard-images/assement-center.png';
            this.model[i].clickfn = "loadAssessmentCenter()";
            this.model[i].block = (parseInt(i) + 1);
          }
          if (this.model[i].componant == "qzch") {
            this.model[i].color = '#e2a21c';
            this.model[i].image = 'assets/images/dashboard-images/quiz-challange-one.png';
            this.model[i].clickfn = "loadQuizChallenge()";
            this.model[i].block = (parseInt(i) + 1);
          }
          if (this.model[i].componant == "pmrw") {
            this.model[i].color = '#007c7c';
            this.model[i].image = 'assets/images/dashboard-images/profermence-review.png';
            this.model[i].clickfn = "loadPerformanceReview()";
            this.model[i].block = (parseInt(i) + 1);
          }
          if (this.model[i].is_enable == false) {
            this.model[i].clickfn = "accessDenied()";
          }

        }


      }

      setTimeout(() => {
        this.tbLoaderService.displayMainLoader(false);
      }, AppSettings.MAIN_LOADER_TIMEOUT);

    }
    else {

    }


  }

  accessDenied() {

  }


}
