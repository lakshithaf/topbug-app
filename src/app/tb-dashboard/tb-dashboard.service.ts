import {Injectable} from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {AppSettings} from '../app.settings';
import 'rxjs/add/operator/map';

const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};
@Injectable()
export class TbDashboardService {

  constructor(private _http: HttpClient) {
  }

  getDashboardComponents(token){

    return this._http.get(AppSettings.ST_PORTAL_API_ENDPOINT + '/dashcomp/'+token,httpOptions)

  }


}
