import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbChallengeComponent } from './tb-challenge.component';

describe('TbChallengeComponent', () => {
  let component: TbChallengeComponent;
  let fixture: ComponentFixture<TbChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
