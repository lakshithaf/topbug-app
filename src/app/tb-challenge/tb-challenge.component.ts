import {Component, OnInit} from '@angular/core';
import {AppSettings} from '../app.settings';
import {TbLoaderService} from '../tb-common/tb-loader/tb-loader.service';
import {TbChallengeService} from './tb-challenge.service';


@Component({
  selector: 'app-tb-challenge',
  templateUrl: './tb-challenge.component.html',
  styleUrls: ['./tb-challenge.component.scss'],
  providers: [TbChallengeService],
  encapsulation:AppSettings.ENCAPSULATE
})
export class TbChallengeComponent implements OnInit {
  public data;
  public model;

  constructor(private tbLoaderService: TbLoaderService,private _service: TbChallengeService) {
  }

  ngOnInit() {

    if (sessionStorage.getItem('sub_comp') == null) {
      let token = localStorage.getItem('token');
      this._service.getSubject(token).subscribe(data => {
        this.data = data;
        this.generateSubjects(this.data);
      })
    }else{
      this.generateSubjects(JSON.parse(sessionStorage.getItem('sub_comp')));
    }

  }

  ngAfterViewInit() {

  }

  generateSubjects(data) {

    if (data.status == 200 && data.data.length > 0) {
      this.model = data.data;
      for (let i in this.model) {

        if (this.model[i].subject_name == "Maths") {
          this.model[i].sub_id = this.model[i].subject_name+"sub";
          this.model[i].img_url = 'assets/images/maths.png';
        }
        if (this.model[i].subject_name == "Biology") {
          this.model[i].sub_id = this.model[i].subject_name+"sub";
          this.model[i].img_url = 'assets/images/bio.png';
        }
        if (this.model[i].subject_name == "Chemistry") {
          this.model[i].sub_id = this.model[i].subject_name+"sub";
          this.model[i].img_url = 'assets/images/chemistry.png';
        }
        if (this.model[i].subject_name == "Physics") {
          this.model[i].sub_id = this.model[i].subject_name+"sub";
          this.model[i].img_url = 'assets/images/physics.png';
        }

      }

      setTimeout(() => {
        this.tbLoaderService.displayMainLoader(false);
      }, AppSettings.MAIN_LOADER_TIMEOUT);

    }
    else{

    }
  }

}
