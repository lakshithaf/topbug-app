import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppSettings} from "../../app.settings";

@Component({
  selector: 'app-tb-login',
  templateUrl: './tb-signup.component.html',
  styleUrls: ['./tb-signup.component.scss'],
  encapsulation:AppSettings.ENCAPSULATE
})
export class TbSignupComponent implements OnInit {

  constructor(private route: Router) {
  }

  ngOnInit() {
  }

  loadDashboard() {

    this.route.navigateByUrl('portal/dashboard');

  }

}
