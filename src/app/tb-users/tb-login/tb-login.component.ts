import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {TbLoginService} from './tb-login.service';
import {IoService} from "../../tb-service/tb-socket.io.service";
import {AppSettings} from "../../app.settings";

@Component({
  selector: 'app-tb-login',
  templateUrl: './tb-login.component.html',
  styleUrls: ['./tb-login.component.scss'],
  providers: [TbLoginService],
  encapsulation:AppSettings.ENCAPSULATE
})
export class TbLoginComponent implements OnInit {

  public model: any = {};
  private user;

  constructor(private route: Router, private router: ActivatedRoute, private _service: TbLoginService) {
  }

  ngOnInit() {
  }

  login() {
    this._service.login(this.model.email_address, this.model.password)
      .subscribe(
        data => {
          this.user = data;
          if (this.user.status == "200" && this.user.Role == "Student") {
            localStorage.setItem('token', this.user.token);
            this.route.navigate(['portal/dashboard']);
          } else {
            alert("Invalid User");
            this.route.navigate(['/']);
          }
        },
        error => {
          console.log(error)
        });

  }

}
