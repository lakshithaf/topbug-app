import {Injectable} from '@angular/core';
import {HttpClient, HttpClientModule, HttpHeaders} from '@angular/common/http';
import {AppSettings} from '../../app.settings';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

@Injectable()
export class TbLoginService {

  constructor(private _http: HttpClient) {
  }

  login(email_address: string, password: string) {

    let data = {email: email_address, password: password};
    return this._http.post(AppSettings.ST_PORTAL_API_ENDPOINT + "/login", data, httpOptions)


  }


}
