import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbLoginComponent } from './tb-login.component';

describe('TbLoginComponent', () => {
  let component: TbLoginComponent;
  let fixture: ComponentFixture<TbLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
