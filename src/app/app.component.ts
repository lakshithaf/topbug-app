import {Component, TemplateRef, AfterViewInit, HostListener} from '@angular/core';
import {AppSettings} from './app.settings';
import {AppService} from './app.service';
import {IoService} from "./tb-service/tb-socket.io.service";
import {TbCommonService} from './tb-service/tb-common.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [IoService],
  encapsulation:AppSettings.ENCAPSULATE
})
export class AppComponent {
  title = 'app';
  public data;

  constructor(private _service:AppService,private userService:TbCommonService) {

  }
  ngOnInit() {

  }
  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    this.userService.endSession().subscribe(data => {
    })
  }

  ngAfterViewInit() {

  }
}
