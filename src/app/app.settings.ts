import {ViewEncapsulation} from "@angular/core";

export class AppSettings {
  public static ST_PORTAL_API_ENDPOINT='http://192.168.1.51/st_portal_latest/public/index.php';
  public static FILE_MANAGER_API_ENDPOINT='http://192.168.1.51:8004/files';
  public static ADMIN_PORTAL_API_ENDPOINT='http://192.168.1.51:8005';
  public static AUTH_TOKEN=localStorage.getItem('authToken');
  public static TOKEN=localStorage.getItem('token');
  public static MAIN_LOADER_TIMEOUT=500;
  public static ENCAPSULATE=ViewEncapsulation.None;
}
