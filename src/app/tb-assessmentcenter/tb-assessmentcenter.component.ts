import {Component, OnInit, ViewChild} from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {AppSettings} from '../app.settings';
import {TbLoaderService} from '../tb-common/tb-loader/tb-loader.service';
import {IoService} from "../tb-service/tb-socket.io.service";
import {TbAssessmentcenterService} from './tb-assessmentcenter.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-tb-assessmentcenter',
  templateUrl: './tb-assessmentcenter.component.html',
  styleUrls: ['./tb-assessmentcenter.component.scss'],
  providers: [TbAssessmentcenterService],
  encapsulation: AppSettings.ENCAPSULATE
})
export class TbAssessmentcenterComponent implements OnInit {
  public data;
  public model;
  public modelSub;
  public modelModule;
  public modalRef: BsModalRef;
  public test_type;
  private token = localStorage.getItem('token');
  public subColor;
  public subject;
  public config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'Loadtests confirm_box'
  };
  public config1 = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'Loadmodules confirm_box'
  };
  public routerEvents;
  public loaderClass: string = "";
  public httpEvents;
  @ViewChild('mainModal') mainModal: any;
  @ViewChild('subModal') subModal: any;


  constructor(private tbLoaderService: TbLoaderService,
              private router: Router,
              private _service: TbAssessmentcenterService,
              private _modalService: BsModalService,
              private activatedRoute: ActivatedRoute) {

    this.routerEvents = this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .subscribe((event) => {
        this.activatedRoute.params.subscribe(params => {
          this.loadRoutes(params)
        });
      });

  }

  ngOnInit() {
    if (sessionStorage.getItem('sub_comp') == null) {
      this.httpEvents = this._service.getSubject(this.token).subscribe(data => {
        this.data = data;
        sessionStorage.setItem('sub_comp', JSON.stringify(this.data));
        this.generateSubjects(this.data)
      });
    } else {
      this.generateSubjects(JSON.parse(sessionStorage.getItem('sub_comp')))

    }


  }

  ngOnDestroy() {
    if (this.routerEvents) {
      this.routerEvents.unsubscribe();
    }

  }

  ngAfterViewInit() {
  }

  loadRoutes(params) {
    if ((params.subId || params.testType) && sessionStorage.getItem('sel_assessub_data') != null) {
      if (params.subId && params.testType) {
        if (params.testType == 'module-test') {
          let sub_data = JSON.parse(sessionStorage.getItem('sel_assessub_data'));
          let data = JSON.parse(sessionStorage.getItem('sel_assessubmodule_data'));
          this.subColor = sub_data.color;
          this.subject = sub_data.subject_name;
          sessionStorage.setItem('type', this.test_type);
          this.loadModuleModal(data);
          this.modalRef = this._modalService.show(this.subModal, Object.assign({}, this.config1));
        }
        else {

        }


      } else {
        let data = JSON.parse(sessionStorage.getItem('sel_assessub_data'));
        this.loadTestModal(data);
        this.modalRef = this._modalService.show(this.mainModal, Object.assign({}, this.config));

      }
    } else {
      console.log("Assesment Center dashbord");
    }

  }

  loadDashboard() {
    this.router.navigateByUrl('portal/dashboard');
  }

  generateSubjects(data) {
    if (data.status == 200 && data.data.length > 0) {
      this.model = data.data;
      for (let i in this.model) {

        if (this.model[i].subject_name == "Maths") {
          this.model[i].bg_color = '#22c8f8';
          this.model[i].img_url = 'assets/images/maths.png';
        }
        if (this.model[i].subject_name == "Biology") {
          this.model[i].bg_color = '#039479';
          this.model[i].img_url = 'assets/images/bio.png';
        }
        if (this.model[i].subject_name == "Chemistry") {
          this.model[i].bg_color = '#e2a21c';
          this.model[i].img_url = 'assets/images/chemistry.png';
        }
        if (this.model[i].subject_name == "Physics") {
          this.model[i].bg_color = '#c13d3c';
          this.model[i].img_url = 'assets/images/physics.png';
        }
        if (this.model[i].subject_avail == 0) {
          this.model[i].onclick = 'disabled_Subject()';
          this.model[i].addClass = 'disabled';
          this.model[i].style = '#ccc';
          this.model[i].stylebg = '#aaa';
          this.model[i].bg_color = '#ccc';
        }
        if (this.model[i].subject_avail != 0) {
          this.model[i].onclick = '';
          this.model[i].addClass = 'hvr-grow materialize';
          this.model[i].style = '';
          this.model[i].stylebg = this.model[i].color;

        }
      }
      setTimeout(() => {
        this.tbLoaderService.displayMainLoader(false);
      }, AppSettings.MAIN_LOADER_TIMEOUT);
    } else {
    }
  }

  loadTestData(data) {

    let isEnable = data.subject_avail;
    if (isEnable == 0) {
      this.noContentAvailable();
    } else {
      sessionStorage.setItem("sel_assessub_data", JSON.stringify(data));
      this.modelSub = data;
      this.router.navigate(['/portal/assessment-center/', this.modelSub.subject_id]);

    }

  }

  loadTestModal(data) {

    let isSubAvailable = data.test_type[0].test_avail;
    let isModAvailable = data.test_type[1].test_avail;
    this.modelSub = data;
    if (isSubAvailable == '0') {
      this.modelSub.disabledsubtest = true;
      this.modelSub.disabledsub = "disabled";
      this.confirmMsgSubjectTest();
    } else if (isModAvailable == '0') {
      this.modelSub.disabledmoduletest = true;
      this.modelSub.disabledmodule = "disabled";
      this.confirmMsg();
    } else {
      this.modelSub.disabledsubtest = false;
      this.modelSub.disabledmoduletest = false;
      this.modelSub.disabledsub = "materialize hvr-grow";
      this.modelSub.disabledmodule = "materialize hvr-grow";

    }

  }

  loadSubTest(data) {
    this.test_type = 'subject-test';
    if (!data.disabledsubtest) {
      sessionStorage.setItem('type', this.test_type);
      this.modalRef.hide();
      this.router.navigate(['/portal/assessment-center/' + this.modelSub.subject_id + '/', this.test_type]);
    } else {
      this.confirmMsgSubjectTest()
    }


  }

  loadModuleResult(data) {
    this.loaderClass = "loader_inner";
    this._service.getModules(this.token, data.subject_id).subscribe(data => {
      this.data = data;
      this.loadModuleData(this.data)
    })

  }

  loadModuleData(data) {
    this.test_type = 'module-test';
    let sub_data = JSON.parse(sessionStorage.getItem('sel_assessub_data'));
    sessionStorage.setItem('sel_assessubmodule_data', JSON.stringify(data));
    this.modelModule = data.data.modules;
    this.loaderClass = "";
    this.modalRef.hide();
    this.router.navigate(['/portal/assessment-center/' + sub_data.subject_id + '/', this.test_type]);

  }

  loadModuleModal(data) {
    this.modelModule = data.data.modules;
    let count = 0, even_row = 0;
    for (let i in this.modelModule) {
      count = parseInt(i);
      if (count == 2 && even_row == 0) {
        even_row = 1;
        count = 0;
      } else if (count == 2 && even_row == 1) {
        even_row = 0;
        count = 0;
      }
      if (even_row == 1) {
        this.modelModule[i].base_color = this.subColor;
      } else {
        this.modelModule[i].base_color = this.subColor;
      }
      if (this.modelModule[i].is_enable == false) {
        this.modelModule[i].disable = 'disabled';
        this.modelModule[i].base_color = '#a5a5a5';
      }
      else {
        this.modelModule[i].disable = '';
      }

    }


  }

  noContentAvailable() {
    alert("No content available")
  }

  confirmMsgSubjectTest() {

  }

  confirmMsg() {

  }

  backToMain() {
    window.history.back();
    this.modalRef.hide();

  }

  loadModuleTest(data) {

  }

  shadeColor1(color, percent) {
    let num = parseInt(color.slice(1), 16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt,
      G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
    return "#" + (0x1000000 + (R < 255 ? R < 1 ? 0 : R : 255) * 0x10000 + (G < 255 ? G < 1 ? 0 : G : 255) * 0x100 + (B < 255 ? B < 1 ? 0 : B : 255)).toString(16).slice(1);
  }


}
