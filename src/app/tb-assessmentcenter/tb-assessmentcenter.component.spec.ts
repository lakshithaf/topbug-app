import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbAssessmentcenterComponent } from './tb-assessmentcenter.component';

describe('TbAssessmentcenterComponent', () => {
  let component: TbAssessmentcenterComponent;
  let fixture: ComponentFixture<TbAssessmentcenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbAssessmentcenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbAssessmentcenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
