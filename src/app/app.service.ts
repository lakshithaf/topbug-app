import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from './app.settings';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {

  constructor(private _http: HttpClient) {
  }

}
