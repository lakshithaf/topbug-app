import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbHomeComponent } from './tb-home.component';

describe('TbHomeComponent', () => {
  let component: TbHomeComponent;
  let fixture: ComponentFixture<TbHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
