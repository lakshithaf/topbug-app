import {Component, OnInit} from '@angular/core';
import {IoService} from "../tb-service/tb-socket.io.service";
import {AppSettings} from "../app.settings";

@Component({
  selector: 'app-tb-home',
  templateUrl: './tb-home.component.html',
  styleUrls: ['./tb-home.component.sass'],
  encapsulation:AppSettings.ENCAPSULATE
})
export class TbHomeComponent implements OnInit {

  constructor(private _ioService: IoService) {
  }

  ngOnInit() {
    this._ioService.subscribeToSails();
  }

}
